<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => ['verified', 'role:administrator']], function (){

	Route::resource('accomodations', 'Admin\AccomodationController');

	Route::resource('images', 'Admin\ImageController');
	
	Route::resource('activities', 'Admin\ActivityController');
});

Route::get('accomodation/{accomodation}', 'AccomodationPagesController@showAccomodation')->name('accomodation.pages.show');

Route::get('accomodation', 'AccomodationPagesController@getAccomodations')->name('accomodation.pages.index');

Route::get('activities/{activity}', 'ActivityPagesController@showActivity')->name('activity.pages.show');

Route::get('activities', 'ActivityPagesController@getActivities')->name('activity.pages.index');

Route::get('/', 'WelcomeController@index')->name('welcome');

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');
