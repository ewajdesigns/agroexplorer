<?php

namespace App\Http\Controllers;

use App\Accomodation;
use Illuminate\Http\Request;

class AccomodationPagesController extends Controller
{
    public function getAccomodations(Request $request)
    {
    	$perPage = $request->_size ? : 10;

    	$accomodations = Accomodation::paginate($perPage);

    	return view('pages.accomodation.index', [
    		'accomodations' => $accomodations
    	]);
    }

    public function showAccomodation(Accomodation $accomodation)
    {
    	return view('pages.accomodation.show', [
    		'accomodation' => $accomodation
    	]);
    }
}
