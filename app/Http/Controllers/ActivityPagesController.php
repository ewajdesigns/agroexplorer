<?php

namespace App\Http\Controllers;

use App\Activity;
use Illuminate\Http\Request;

class ActivityPagesController extends Controller
{
    public function getActivities(Request $request)
    {
    	$perPage = $request->_size ? : 10;

    	$activities = Activity::paginate($perPage);

    	return view('pages.activities.index', [
    		'activities' => $activities
    	]);
    }

    public function showActivity(Activity $activity)
    {
    	return view('pages.activities.show', [
    		'activity' => $activity
    	]);
    }
}
