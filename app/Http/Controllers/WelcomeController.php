<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelcomeController extends Controller
{
	// show the application welcome page
    public function index()
    {
    	return view('welcome');
    }
}
