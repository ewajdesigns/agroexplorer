<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Accomodation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateAccomodationRequest;
use App\Http\Requests\UpdateAccomodationRequest;

class AccomodationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = $request->_size ? : 10;

        $accomodations = Accomodation::paginate($perPage);

        return view('admin.accomodation.index', [
            'accomodations' => $accomodations
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.accomodation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateAccomodationRequest $request)
    {
        $user = Auth::user();

        $accomodation = $user->createAccomodation($request);

        return redirect()->route('accomodations.show', $accomodation->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Accomodation $accomodation)
    {
        return view('admin.accomodation.show', [
            'accomodation' => $accomodation
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Accomodation $accomodation)
    {
        return view('admin.accomodation.update', [
            'accomodation' => $accomodation
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAccomodationRequest $request, Accomodation $accomodation)
    {
        $user = Auth::user();

        $user->updateAccomodation($request, $accomodation);

        return redirect()->route('accomodations.show', $accomodation->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Accomodation $accomodation)
    {
        $user = Auth::user();

        $user->deleteAccomodation($accomodation);

        return redirect()->route('accomodations.index');
    }
}
