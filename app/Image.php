<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = [
        'url', 'path'
    ];

    // the user relationship
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // save the uploaded image to storage
    public function saveToStorageAs($request, $id)
    {
        return "path/to/images/$id.jpg";
    }

    // delete an image from storage
    public function deleteFromStorage()
    {
        // Storage::detele($this->path);
        return true;
    }

    // completely remove the image from db and storage
    public function wipe()
    {
        DB::beginTransaction();

        $this->delete();

        $this->deleteFromStorage();

        DB::commit();
    }
}
