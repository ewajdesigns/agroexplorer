<?php

namespace App;

use Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laratrust\Traits\LaratrustUserTrait;

class User extends Authenticatable implements MustVerifyEmail
{
    use LaratrustUserTrait;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    // App\Activity relation
    public function activities()
    {
        return $this->hasMany(Activity::class);
    }

    public function images()
    {
        return $this->hasMany(Image::class);
    }

    public function accomodations()
    {
        return $this->hasMany(Accomodation::class);
    }

    // create a new activity and save to db
    public function createActivity($request)
    {
        // $activity = new activity
        // $activity->update($request->only($activity_fillable));
        // return $activity
    }

    // update the specified activity and save to db
    public function updateActivity($request, $activity)
    {
        # $activity->update($request->only($activivty_fillable));
        # return $activity
    }

    // destroy the specified activity
    public function destroyActivity($activity)
    {
        # $activity->delete();
        # remove associations with gallery images
        # return true
    }

    // 
    public function saveImage($request, $image = null)
    {
        DB::beginTransaction();

        $image = $image ?? $this->images()->create();

        $path = $image->saveToStorageAs($request, $image->id);

        $url = "http://web/location/" . $image->id . ".jpg";

        $image->update([
            'path' => $path,
            'url' => $url
        ]);

        DB::commit();
        
        return $image;
    }

    public function updateImage($request, $image)
    {
        $image->deleteFromStorage();
        
        return $this->saveImage($request, $image);
    }

    public function createAccomodation($request)
    {
        $ac = $this->accomodations()->create($request->all());
        return $ac;
    }

    public function deleteAccomodation($accomodation)
    {
        return true;
    }
}
