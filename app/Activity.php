<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
	protected $fillable = [
		'name', 'duration', 'quantity', 'price', 'date', 'description'
	];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
