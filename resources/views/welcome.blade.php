<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>AgroExplorer</title>

     <meta name="viewport" content="width=device-width, initial-scale=1.0">

<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/shopfrog.css" rel="stylesheet" media="screen">   
<link href="css/shopfrog-bw.css" rel="stylesheet" media="screen">

<link href="css/rateit.css" rel="stylesheet" media="screen">               
<link href="css/magnific-popup.css" rel="stylesheet">       
<script src="js/respond.min.js"></script>
<link href="favicon.png" rel="shortcut icon" type="image/x-icon" />
<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="js/code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="js/modernizr.min.js"></script> 
<script src="js/imagesloaded.min.js"></script>  
<script src="js/jquery.masonry.min.js"></script>    
<script src="js/jquery.rateit.min.js"></script>     
<script src="js/jquery.magnific-popup.min.js"></script>             
<script src="js/bootstrap.js"></script>
<script src="js/shopfrog.js"></script>


</head>
<body class="bodymain product-board">
    
    <header class="container-fluid navbar navbar-fixed-top clearfix">
        
    <a style="font-weight: bold;padding-top: 10px;padding-bottom: 10px;margin-top: 5px;" class="current brand" href="{{ route('welcome') }}">AgroExplorer</a>

    
    <button type="button" class="btn navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>

    <nav class="navbar-collapse collapse navbar-right main-header" id="main-nav">
        <ul class="nav">
            <li class="current active">
                <a href="#" class="current top-level flat">How It Works!</a>
            </li>
            <li >
                <a href="#" class="current top-level flat">Support</a>
            </li>
            <li>
                <a href="{{ route('register') }}" class="top-level flat">Sign up</a>
            </li>
            <li>
                <a style="background-color: #ffc000;color: #ffff;font-weight: bold;padding-top: 10px;padding-bottom: 10px;margin-top: 10px;" href="{{ route('login') }}" class="top-level flat">Log in</a>
            </li>
        </ul>
    </nav><!--/.nav-collapse -->
    
</header>


    
    <div id="product-board" class="clearfix">
        <div class="container">
            <div class="row">
                <div class="cardib col-md-offset-3 col-md-6 col-xs-12">
                    <div class=" product large" style="position: absolute; left: 0px; top: 0px;margin-top: 100px;">
                        <div class="media text-center">
                            <br>
                            <button style="border-radius: 0;background-color: #bfbfbf;font-weight: bold;" class="btn btn-lg btn-success">AgroExplorer</button>
                            <h1>Enjoy the best of</h1>
                            <h2>Victoria falls for less </h2>
                            <form >
                                <div class="input-group form-group" style="width: 80%;padding-left: 19%;border-radius: 2px;">
                                    <input class="form-control" type="date" placeholder="Pick your travelling date">
                                        <span class="input-group-addon"><img src="img/calender.PNG"></span>
                                </div>
                                <div class="form-group">
                                <button style="width: 62.0%;height: 54px;font-weight: bold;background-color:#ffc000;border-radius: 2px;border: none;" class="btn btn-lg btn-success">Explore</button>
                            </div>
                            </form>
                        </div>
                        <div class="details" style="background-color: #bfbfbf;padding: 10px 0;position: relative;">
                            <div class="row">
                                <div class="col-xs-2">
                                    <img style="padding-left:30px" src="img/gift.PNG">
                                </div>
                                <div class="col-xs-10">
                                    <p class="name">Get a free souvenir for your first
                                    holiday! </p>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    
</body>

</html>